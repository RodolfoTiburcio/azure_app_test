# Python

# Pydantic (Tipos de datos)
from pydantic import BaseModel

# fastAPI
from fastapi import APIRouter

class User(BaseModel):
    email:str
    password:str