# Python
from typing import List

# Pydantic (Tipos de datos)
from pydantic import BaseModel, Field

class Project(BaseModel):
    title: str = Field()
    client: str = Field()
    status: str = Field()