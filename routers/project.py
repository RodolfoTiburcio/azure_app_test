# Python
from typing import List

# Pydantic (Tipos de datos)
from pydantic import BaseModel, Field

# fastAPI
from fastapi import Query, Depends, Path, APIRouter
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

# app
from config.database import Session, engine, Base
from models.project import Project as ProjectModel
from middlewares.jwt_bearer import JWTBearer
from services.project import ProjectService
from schemas.project import Project

Base.metadata.create_all(bind=engine)

project_router = APIRouter()

@project_router.get(
    '/project',
    tags=['Proyectos'],
    response_model = List[Project],
    status_code=200,
    #dependencies=[Depends(JWTBearer())]
)
def get_projects() -> List[Project]:
    db = Session()
    result = ProjectService(db).get_projects()
    return JSONResponse(
        status_code=200,
        content=jsonable_encoder(result)
    )

@project_router.get(
    '/project/{id}',
    tags=['Proyectos'],
    response_model=Project
)
def get_project(
    id: int = Path(ge=1, le=2000)
) -> Project:
    db = Session()
    result = ProjectService(db).get_project(id)
    if not result:
        return JSONResponse(
            status_code=404,
            content= {"message":"No encontrado"}
        )
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@project_router.get(
    '/project/',
    tags=['Proyectos'],
    response_model = Project
)
def get_project_by_client(
    client: str = Query(min_length=2, max_length=50)
) -> List[Project]:
    db = Session()
    result = ProjectService(db).get_project_by_client(client)
    if not result:
        return JSONResponse(
            status_code=404,
            content={"message":"No encontrado"}
        )
    return JSONResponse(
        status_code=200,
        content=jsonable_encoder(result)
    )

@project_router.post(
    '/project',
    tags=['Proyectos'],
    response_model=dict,
    status_code=201

)
def create_project(project: Project) -> dict:
    db = Session()
    ProjectService(db).create_project(project)
    return JSONResponse(
        status_code=201,
        content={"message":"Se ha registrado el proyecto"}
    )

@project_router.put(
    '/projects/{id}',
    tags=['Proyectos'],
    response_model = dict,
    status_code=200
)
def update_project(
    id: int,
    project: Project
) -> dict:
    db = Session()
    result = ProjectService(db).get_project(id)
    if not result:
        return JSONResponse(
            status_code=404,
            content={"message":"No encontrado"}
        )
    ProjectService(db).update_project(id, project)
    return JSONResponse(
        status_code=200,
        content={"message":"Se ha modificado el proyecto"}
    )

@project_router.delete(
    '/project/{id}',
    tags=['Proyectos'],
    response_model=dict,
    status_code=200
)
def delete_project(id: int) -> dict:
    db = Session()
    result = ProjectService(db).get_project(id)
    if not result:
        return JSONResponse(
            status_code=404,
            content={'message': 'No encontrado'}
        )
    ProjectService(db).delete_project(id)
    return JSONResponse(
        status_code=200,
        content={"message": "Se ha eliminado el proyecto"}
    )