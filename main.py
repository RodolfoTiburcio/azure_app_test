# fastAPI
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

# app
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.project import project_router
from routers.user import user_router

# deploy
import uvicorn

Base.metadata.create_all(bind=engine)

app = FastAPI()
app.title = "Lista de proyectos"
app.version = "0.0.1"

origins = ["*"]

app.add_middleware(ErrorHandler)
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.include_router(project_router)
app.include_router(user_router)

if __name__ == '__main__':
    uvicorn.run('myapp:app', host='0.0.0.0', port=8000)