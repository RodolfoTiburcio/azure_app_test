"""add status to project

Revision ID: 3985f67f7a6c
Revises: 
Create Date: 2023-01-31 03:40:33.121100

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3985f67f7a6c'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        table_name="",
        column="test",
    )


def downgrade() -> None:
    pass
