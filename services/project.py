from models.project import Project as ProjectModel
from schemas.project import Project

class ProjectService():

    def __init__(self, db) -> None:
        self.db = db

    def get_projects(self):
        result = self.db.query(ProjectModel).all()
        return result

    def get_project(self, id):
        result = self.db.query(ProjectModel).filter(
            ProjectModel.id == id
        ).first()
        return result

    def get_project_by_client(self, client):
        result = self.db.query(ProjectModel).filter(
            ProjectModel.client == client
        ).all()
        return result

    def create_project(self, project: Project):
        new_project = ProjectModel(**project.dict())
        self.db.add(new_project)
        self.db.commit()
        return

    def update_project(self, id:int, data: Project):
        project = self.db.query(ProjectModel).filter(
            ProjectModel.id == id
        ).first()
        project.title = data.title
        project.client = data.client
        self.db.commit()
        return

    def delete_project(self, id: int):
        self.db.query(ProjectModel).filter(
            ProjectModel.id == id
        ).delete()
        self.db.commit()
        return